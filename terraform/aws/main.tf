resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQ7ol9AP7TEETicFWcWmMYNUlhpopuglbuRDSbXyL/8Q2ELBDCjRhFB3JfoCqyDF3QReZyacNzjY3BnTVF2J4lg4MoafzFU3uJTbPCm6D4HbDExX+RCqMlGHzwdhXwcpxv0LXcnNvEeqO2sld8xqQ+GBU2LWSokg9vnFWSNoJ+fxuVNn0d71iWL7A881XZzLCY3QES1LGEWkON8riXakFtE7ODVIh1ySAtgl5WQcpc2JvIb/8SDPqGZm0ivEEfaDqIfyAoZCOD/t5wl6jq1A/0f5RMvFLA10tiFvBAmX8RtuY9mrw8+V0vEB+t1wtCshJIGSMNqRQ4S3rmkNpq1vdTq0G0GAuyJT4pDZJoDsA+5xfWjMOtVcfSfw2XKGdgvKul5NvWEJ+ah6On5kyhn8rxulLLTWB6pdzh+NPugKAFyyJPfHpyuKOlSGlzQxt0KRmqT+OwHgreISID4/6RZkbnC/oozMmcygl9cZ9m45mpSVJhZWdfJ7+OpIQ06Riiba8= solene@toshibac650"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "default-vpc"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5000
    to_port   = 5000
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  key_name = "deployer-key"
  vpc_security_group_ids = [aws_default_security_group.default.id]

  tags = {
    Name = "HelloWorld"
  }
}

